import time

import pytest
from brownie import DutchAuction, chain

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

_START_DATE = int(time.time()) - _ONE_DAY_IN_SECONDS
_END_DATE = int(time.time()) + _ONE_DAY_IN_SECONDS


@pytest.fixture
def start_price():
    return 100


@pytest.fixture()
def account_A(accounts):
    return accounts[0]


@pytest.fixture()
def account_B(accounts):
    return accounts[1]


@pytest.fixture()
def account_C(accounts):
    return accounts[2]


@pytest.fixture()
def _auction(account_A, start_price):
    _auction = DutchAuction.deploy(
        _START_DATE,
        _END_DATE,
        start_price,
        {"from": account_A},
    )
    return _auction


@pytest.fixture()
def expired_auction(account_A, start_price):
    _auction = DutchAuction.deploy(
        _START_DATE, _START_DATE + 1, start_price, {"from": account_A}
    )
    return _auction
