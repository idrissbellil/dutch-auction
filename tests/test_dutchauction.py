import time

import brownie


def test_initial_state(_auction, start_price):
    assert _auction.startPrice() == start_price
    assert int(time.time()) > _auction.startDate()
    assert int(time.time()) < _auction.endDate()


def test_get_price(_auction, start_price):
    assert _auction.getPriceNow() < start_price


def test_bid(_auction, account_A, account_B, start_price, expired_auction):
    with brownie.reverts("The auction has expired."):
        expired_auction.bid({"from": account_B, "amount": "10 ether"})

    with brownie.reverts("Transferred amount is lower than current price."):
        _auction.bid({"from": account_B, "amount": "0 ether"})

    assert not _auction.ended()
    trx = _auction.bid({"from": account_B, "amount": "10 ether"})
    assert _auction.ended()
    assert _auction.bidPrice() < start_price
    assert _auction.bidAddress() == account_B
    assert trx.events[0]["bidder"] == account_B

    with brownie.reverts("The auction has ended."):
        _auction.bid()
