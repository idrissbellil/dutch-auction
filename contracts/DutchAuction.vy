# @version ^0.3.1


startDate: public(uint256)
endDate: public(uint256)
startPrice: public(uint256)

ended: public(bool)
bidPrice: public(uint256)
bidAddress: public(address)


# Define the Auction end event
event AuctionEnded:
  bidder: address
  bid: uint256


@external
def __init__(_start_date: uint256, _end_date: uint256, _start_price: uint256):
  assert _start_date < _end_date, "start date is greated than end date"
  self.startDate = _start_date
  self.endDate = _end_date
  self.startPrice = _start_price
  self.ended = False


@internal
@view
def _getPriceAt(_timestamp: uint256) -> uint256:
  assert _timestamp < self.endDate, "The auction has expired."
  ret: uint256 = (self.endDate - _timestamp) * self.startPrice * 10000000 / \
                 (self.endDate - self.startDate) / 10000000
  return ret


@external
@view
def getPriceNow() -> uint256:
  return self._getPriceAt(block.timestamp)


@external
@view
def getPriceAt(_timestamp: uint256) -> uint256:
  return self._getPriceAt(_timestamp)


@external
@payable
def bid() -> bool:
  _timestamp: uint256 = block.timestamp
  assert _timestamp < self.endDate, "The auction has expired."
  assert not self.ended, "The auction has ended."
  price: uint256 = self._getPriceAt(_timestamp)
  assert msg.value >= price, "Transferred amount is lower than current price."

  # Carry out the asset tranfer
  self.ended = True
  self.bidPrice = price
  self.bidAddress = msg.sender
  log AuctionEnded(msg.sender, price)
  
  # Refund the difference
  if msg.value > price:
    difference: uint256 = msg.value - price
    send(msg.sender, difference)

  return True
